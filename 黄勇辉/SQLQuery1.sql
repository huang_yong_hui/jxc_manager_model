create database invoicing;
go
use invoicing;
go
/*==============================================================*/
/* Table: 进货表                                                   */
/*==============================================================*/
create table 进货表 
(
   buyId                int                            not null,
   GoodNo               varchar(20)                    null,
   supplierName         nvarchar(50)                   null,
   Price                Float                          null,
   Number               int                            null,
   totalPrice           Float                          null,
   buyDate              Datetime                       null,
   addTime              Datetime                       null,
   constraint PK_进货表 primary key clustered (buyId)
);

/*==============================================================*/
/* Table: 商品信息表                                                 */
/*==============================================================*/
create table 商品信息表 
(
   goodNo               varchar(20)                    not null,
   buyId                int                            null,
   "customer number"    nvarchar(30)                   null,
   goodClassId          int                            null,
   goodName             nvarchar(30)                   null,
   goodUnit             nvarchar(2)                    null,
   goodmodel            nvarchar(20)                   null,
   goodSpecs            nvarchar(20)                   null,
   goodPrice            Float                          null,
   goodPlace            nvarchar                       null,
   goodmemo             char(10)                       null,
   goodAddTime          DateTime                      null,
   constraint PK_商品信息表 primary key clustered (goodNo)
);
/*==============================================================*/
/* Table: 库存信息表                                                 */
/*==============================================================*/
create table 库存信息表 
(
   goodNo               varchar(20)                    not null,
   goodCount            int                            null,
   constraint PK_库存信息表 primary key clustered (goodNo)
);
/*==============================================================*/
/* Table: 商品销售表                                                 */
/*==============================================================*/
create table 商品销售表 
(
   sellInfoId           int                            not null,
   sellNo               varchar(30)                     null,
   goodNo               varchar(20)                    null,
   Price                Float                          null,
   Number               int                            null,
   totalPrice           Float                          null,
   sellTime             Datetime                       null,
   employeeNo           varchar                        null,
   constraint PK_商品销售表 primary key clustered (sellInfoId)
);
/*==============================================================*/
/* Table: 供应商信息表                                                */
/*==============================================================*/
create table 供应商信息表 
(
   supplierName         nvarchar(50)                   not null,
   商品编号                 int                            null,
   supplierLawyer       nvarchar(4)                    null,
   supplierTelephone    varchar(11)                    null,
   supplierAddress      nvarchar(50)                   null,
   constraint PK_供应商信息表 primary key clustered (supplierName)
);
/*==============================================================*/
/* Table: 新办客户管理                                                */
/*==============================================================*/
create table 新办客户管理 
(
   "customer number"    nvarchar(30)                   not null,
   supplierName         int                            null,
   IdName               int                            null,
   gender               Float                          null,
   identification       varchar(30)                    null,
   Phone                varchar(20)                    null,
   constraint PK_新办客户管理 primary key clustered ("customer number")
);

/*==============================================================*/
/* Table: 商品信息管理表                                               */
/*==============================================================*/
create table 商品信息管理表 
(
   supplierName         int                            not null,
   commodity            nvarchar(20)                   null,
   quantity             nvarchar(30)                   null,
   unitPrice            nvarchar(5)                    null,
   money                money                          null,
   constraint PK_商品信息管理表 primary key clustered (supplierName)
);
/*==============================================================*/
/* Table: 商品出库统计表                                               */
/*==============================================================*/
create table 商品出库统计表 
(
   商品编号                 int                            not null,
   supplierName         int                            null,
   clienttype           Float                          null,
   deliveery            nvarchar(20)                   null,
   unitprice            Float                          null,
   money                money                          null,
   Datetime             Datetime                       null,
   constraint PK_商品出库统计表 primary key clustered (商品编号)
);

/*==============================================================*/
/* Table: 退货信息表                                                 */
/*==============================================================*/
create table 退货信息表 
(
   ProductID            varchar(30)                    not null,
   ReasonForReturn      varchar(30)                   null,
   RefundThePrice       money                          null,
   PurchaserNumber      varchar(11)                    null,
   EmployeeId           nvarchar(20)                   null,
   constraint PK_退货信息表 primary key clustered (ProductID)
);
/*==============================================================*/
/* Table: 员工信息表                                                 */
/*==============================================================*/
create table 员工信息表 
(
   EmployeeId           nvarchar(20)                   not null,
   HomeAddress          nvarchar(80)                   null,
   ContactNumber        varchar(11)                    null,
   CurrentStatus        varchar(10)                    null,
   constraint PK_员工信息表 primary key clustered (EmployeeId)
);
