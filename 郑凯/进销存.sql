--创建管理系统
create database MangementSystem

use MangementSystem
--创建货源表
create table Supply 
(
	BusinessName nvarchar(25),
	BusinessAddress nvarchar(100),
	BusinessPhone int
)
--创建采购表
create table Procurement
(
	GoodsId int,               --商品编码
	GoodsName nvarchar(25),    --商品名字
	GoodsType int,             --商品型号
	GoodsAmount int,           --商品数量
	GoodsPrice money,          --商品单价
	TotalPrice money,          --商品总价
)
--创建入库表
create table InWareHouse
(
	GoodsId int,               --商品编码
	GoodsName nvarchar(25),    --商品名字
	GoodsType int,             --商品型号
	GoodsAmount int,           --商品数量
)
--创建商品报价表
create table GoodsQuoted 
( 
	GoodsId int,               --商品编码
	GoodsName nvarchar(25),    --商品名字
	GoodsType int,             --商品型号
	GoodsPrice money,          --商品单价
)
--创建销售渠道表
create table Channel
(
	Online int,                --线上销售
	Shop int,                  --门店销售

)
--创建销售对象表
create table Object 
(
	Older int,                 --老年人群
	Teenager int,              --青少年人群
	Children int,              --小孩人群
	Women int,                 --女性
	Men int,                   --男性

)

--创建商品销售表
create table Market
(
	GoodsId int,               --商品编码
	GoodsType int,             --商品型号
	GoodsSellAmount int,       --商品销售数量
	GoodsPrice money,          --商品单价
	TotalPrice money,          --商品总价
	OwePrice money,            --商家拖欠金额
)
--创建售后表
create table AfterSale
( 
	GoodsId int,               --商品编码
	GoodsName nvarchar(25),    --商品名字
	GoodsType int,             --商品型号
	GoodsReturn int,           --商品退货
	ReturnWhy nvarchar(100),   --退货原因
	Compensate nvarchar(100)   --补偿方式
)
--创建季度商品表
create table Quarter
(
	GoodsId int,               --商品编码
	GoodsName nvarchar(25),    --商品名字
	GoodsType int,             --商品型号
	GoodsSutplus int,          --商品剩余总数
	GoodsReturn int,           --商品退货数量
	GoodsDamage int,           --损坏数量
	
) 

--创建部门表
create table Department
(
	Finance nvarchar(20),     --财务管理
	Warehouse nvarchar(20),   --仓库管理
	CustomerService nvarchar(20), --客服

)

