create database Commodity

use Commodity

--系统用户表
create table SystemUser
(
	UserId int primary key identity (1,1),-- 系统用户Id
	UserName varchar(30),-- 用户名
	UserPwd varchar(16),  -- 用户密码
	UserPhone varchar(20) --用户电话
)

--供应商信息表
create table SupplierInfomation
(
	SupplierId int primary key identity (1,1),-- 供应商Id
	SupplierName varchar(30),-- 供应商名称
	SupplierPrincipal varchar(30),-- 供应商负责人
	SupplierPhone varchar(20),-- 供应商联系方式
	SupplierAddress varchar(50) -- 供应商地址
)

--商品进货表
create table Purchases
(
	PurchasesId  int primary key identity (1,1),-- 商品Id
	SupplierId int ,-- 供应商Id
	PurchasesName varchar(20),-- 商品名
	PurchasesType varchar(30),-- 商品类型
	PurchasesPrice money, -- 商品进货单价
	PurchasesAmount int, -- 进货数量
	PurchasesOdd varchar(20), -- 进货单号
	PurchaseDateTime smalldatetime -- 进货时间 
)

--商品退货表
create table ReturnGood
(
	ReturnGoodId int primary key identity (1,1),-- 退货Id
	PurchasesId int ,-- 商品Id
	ReturnGoodOdd varchar(20), -- 退货单号
	ReturnGoodPrice money ,-- 退货价格
	ReturnGoodWhy varchar(300),--退货原因
	ReturnGoodTime smalldatetime -- 退货时间
)

-- 客户表
create table Client
(
	ClientId int primary key identity (1,1),-- 客户编号
	ClientName varchar(20),-- 客户名
	ClientSupervisor varchar(20),-- 客户负责人
	ClientSupervisorPhone varchar(20),-- 负责人联系方式
	ClientAddress varchar(50)-- 客户地址                                                                                                                          
)

-- 销售表
create table Market
(
	MarketId int primary key identity (1,1),-- 销售编号
	ClientId int ,-- 客户编号
	MarketGoodsId int,-- 销售商品Id
	MarketName varchar(50),-- 销售商品名
	MarketType varchar(20),-- 销售商品类型
	MarketPrice money,--销售单价
	MarketAmount int,-- 销售数量
	MarketTime smalldatetime -- 销售时间
)

-- 退货表
create table ClientReturns
(
	ClientReturns int primary key identity (1,1),-- 客户退货编号
	MarketGoodsId int,-- 销售商品Id
	ClientReturnsOdd varchar(20),-- 退货编号
	ClientReturnsPrice money, -- 退货价格
	ClientReturnsWhy varchar(300),-- 退货原因
	ClientReturnsTime smalldatetime -- 退货时间
)

--交易信息表
create table Deal
(
	DealId int primary key identity (1,1),-- 交易编号
	Market int , --销售Id
	MarketGoodsId int,-- 销售商品Id
	MarketName varchar(50),-- 销售商品名
	MarketPrice money,--销售单价
	MarketAmount int,-- 销售数量
	ClientAddress varchar(50),-- 客户地址  
	DealTime smalldatetime -- 交易时间
)

-- 库存表
create table Inventory
(
	InventoryId int primary key identity (1,1),-- 库存编号
	MarketGoodsId int,-- 销售商品Id
	MarketName varchar(50),-- 销售商品名
	PurchasesPrice money, -- 商品进货单价
	MarketPrice money,--销售单价
	InventoryAmount int ,-- 库存数量
	ClientName varchar(20),-- 客户名

)

